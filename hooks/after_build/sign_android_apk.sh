#!/bin/bash

echo "********************************************************"
echo "signing apk"
echo "********************************************************"

path=`dirname $0`

file=platforms/android/build/outputs/apk/android-release-unsigned.apk
target=HelloWorld.apk

cd $path/../../ \
	&& rm -f ${target} \
	&& echo jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 \
		-keystore ${path}/../my-release-key.jks ${file} my-alias \
	&& jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 \
		-storepass changeit -keystore ${path}/../my-release-key.jks ${file} my-alias \
	&& echo zipalign -v 4 ${file} ${target} \
	&& zipalign -v 4 ${file} ${target} \
	&& echo "***************************************" \
	&& echo "Check out file ${target}" \
	&& echo "***************************************"

rc=$?
exit $rc

